<?php
if(!isset($_GET["id"])){
	echo "Error";
	exit(0);
}
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "iphone";
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
$sql = "SELECT id, name, price FROM iphone where id=".intval($_GET["id"]);
$result = $conn->query($sql);
if ($result->num_rows != 1) {
	echo "Error";
	exit(0);
}
$row = $result->fetch_assoc();
?>
<!DOCTYPE html>
<!-- saved from url=(0050)https://hieuapple.com/product/iphone-5se-16gb.html -->
<html lang="vi" prefix="og: http://ogp.me/ns#"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="https://hieuapple.com/xmlrpc.php">

<title><?=$row["name"]?></title>

<!-- This site is optimized with the Yoast SEO plugin v8.4 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="https://hieuapple.com/product/iphone-5se-16gb.html">
<meta property="og:locale" content="vi_VN">
<meta property="og:type" content="article">
<meta property="og:title" content="iPhone SE 16GB - Hiếu Apple">
<meta property="og:url" content="https://hieuapple.com/product/iphone-5se-16gb.html">
<meta property="og:site_name" content="Hiếu Apple">
<meta property="article:publisher" content="https://www.facebook.com/HieuAppleBH">
<meta property="og:image" content="https://hieuapple.com/wp-content/uploads/2018/11/192-g-se.jpg">
<meta property="og:image:secure_url" content="https://hieuapple.com/wp-content/uploads/2018/11/192-g-se.jpg">
<meta property="og:image:width" content="400">
<meta property="og:image:height" content="400">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="iPhone SE 16GB - Hiếu Apple">
<meta name="twitter:image" content="https://hieuapple.com/wp-content/uploads/2018/11/192-g-se.jpg">
<script type="text/javascript" async="" src="./detail1_files/analytics.js.download"></script><script id="facebook-jssdk" src="./detail1_files/xfbml.customerchat.js.download"></script><script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"Person","url":"https:\/\/hieuapple.com\/","sameAs":["https:\/\/www.facebook.com\/HieuAppleBH"],"@id":"#person","name":"Hieu"}</script>
<!-- / Yoast SEO plugin. -->

<link rel="dns-prefetch" href="https://fonts.googleapis.com/">
<link rel="dns-prefetch" href="https://cdn.jsdelivr.net/">
<link rel="dns-prefetch" href="https://s.w.org/">
<link rel="alternate" type="application/rss+xml" title="Dòng thông tin Hiếu Apple »" href="https://hieuapple.com/feed">
<link rel="alternate" type="application/rss+xml" title="Dòng phản hồi Hiếu Apple »" href="https://hieuapple.com/comments/feed">
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/hieuapple.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script><script src="./detail1_files/wp-emoji-release.min.js.download" type="text/javascript" defer=""></script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel="stylesheet" id="menu-image-css" href="./detail1_files/menu-image.css" type="text/css" media="all">
<link rel="stylesheet" id="photoswipe-css" href="./detail1_files/photoswipe.css" type="text/css" media="all">
<link rel="stylesheet" id="photoswipe-default-skin-css" href="./detail1_files/default-skin.css" type="text/css" media="all">
<style id="woocommerce-inline-inline-css" type="text/css">
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel="stylesheet" id="yith_wccl_frontend-css" href="./detail1_files/frontend.css" type="text/css" media="all">
<link rel="stylesheet" id="yith-quick-view-css" href="./detail1_files/yith-quick-view.css" type="text/css" media="all">
<style id="yith-quick-view-inline-css" type="text/css">

				#yith-quick-view-modal .yith-wcqv-main{background:#ffffff;}
				#yith-quick-view-close{color:#cdcdcd;}
				#yith-quick-view-close:hover{color:#ff0000;}
</style>
<link rel="stylesheet" id="storefront-style-css" href="./detail1_files/style.css" type="text/css" media="all">
<style id="storefront-style-inline-css" type="text/css">

			.main-navigation ul li a,
			.site-title a,
			ul.menu li a,
			.site-branding h1 a,
			.site-footer .storefront-handheld-footer-bar a:not(.button),
			button.menu-toggle,
			button.menu-toggle:hover,
			.handheld-navigation .dropdown-toggle {
				color: #ffffff;
			}

			button.menu-toggle,
			button.menu-toggle:hover {
				border-color: #ffffff;
			}

			.main-navigation ul li a:hover,
			.main-navigation ul li:hover > a,
			.site-title a:hover,
			a.cart-contents:hover,
			.site-header-cart .widget_shopping_cart a:hover,
			.site-header-cart:hover > li > a,
			.site-header ul.menu li.current-menu-item > a {
				color: #ffffff;
			}

			table th {
				background-color: #f8f8f8;
			}

			table tbody td {
				background-color: #fdfdfd;
			}

			table tbody tr:nth-child(2n) td,
			fieldset,
			fieldset legend {
				background-color: #fbfbfb;
			}

			.site-header,
			.secondary-navigation ul ul,
			.main-navigation ul.menu > li.menu-item-has-children:after,
			.secondary-navigation ul.menu ul,
			.storefront-handheld-footer-bar,
			.storefront-handheld-footer-bar ul li > a,
			.storefront-handheld-footer-bar ul li.search .site-search,
			button.menu-toggle,
			button.menu-toggle:hover {
				background-color: #000000;
			}

			p.site-description,
			.site-header,
			.storefront-handheld-footer-bar {
				color: #ffffff;
			}

			.storefront-handheld-footer-bar ul li.cart .count,
			button.menu-toggle:after,
			button.menu-toggle:before,
			button.menu-toggle span:before {
				background-color: #ffffff;
			}

			.storefront-handheld-footer-bar ul li.cart .count {
				color: #000000;
			}

			.storefront-handheld-footer-bar ul li.cart .count {
				border-color: #000000;
			}

			h1, h2, h3, h4, h5, h6 {
				color: #333333;
			}

			.widget h1 {
				border-bottom-color: #333333;
			}

			body,
			.secondary-navigation a,
			.onsale,
			.pagination .page-numbers li .page-numbers:not(.current), .woocommerce-pagination .page-numbers li .page-numbers:not(.current) {
				color: #6d6d6d;
			}

			.widget-area .widget a,
			.hentry .entry-header .posted-on a,
			.hentry .entry-header .byline a {
				color: #727272;
			}

			a  {
				color: #96588a;
			}

			a:focus,
			.button:focus,
			.button.alt:focus,
			.button.added_to_cart:focus,
			.button.wc-forward:focus,
			button:focus,
			input[type="button"]:focus,
			input[type="reset"]:focus,
			input[type="submit"]:focus {
				outline-color: #96588a;
			}

			button, input[type="button"], input[type="reset"], input[type="submit"], .button, .added_to_cart, .widget a.button, .site-header-cart .widget_shopping_cart a.button {
				background-color: #eeeeee;
				border-color: #eeeeee;
				color: #333333;
			}

			button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .button:hover, .added_to_cart:hover, .widget a.button:hover, .site-header-cart .widget_shopping_cart a.button:hover {
				background-color: #d5d5d5;
				border-color: #d5d5d5;
				color: #333333;
			}

			button.alt, input[type="button"].alt, input[type="reset"].alt, input[type="submit"].alt, .button.alt, .added_to_cart.alt, .widget-area .widget a.button.alt, .added_to_cart, .widget a.button.checkout {
				background-color: #333333;
				border-color: #333333;
				color: #ffffff;
			}

			button.alt:hover, input[type="button"].alt:hover, input[type="reset"].alt:hover, input[type="submit"].alt:hover, .button.alt:hover, .added_to_cart.alt:hover, .widget-area .widget a.button.alt:hover, .added_to_cart:hover, .widget a.button.checkout:hover {
				background-color: #1a1a1a;
				border-color: #1a1a1a;
				color: #ffffff;
			}

			.pagination .page-numbers li .page-numbers.current, .woocommerce-pagination .page-numbers li .page-numbers.current {
				background-color: #e6e6e6;
				color: #636363;
			}

			#comments .comment-list .comment-content .comment-text {
				background-color: #f8f8f8;
			}

			.site-footer {
				background-color: #000000;
				color: #ffffff;
			}

			.site-footer a:not(.button) {
				color: #ffffff;
			}

			.site-footer h1, .site-footer h2, .site-footer h3, .site-footer h4, .site-footer h5, .site-footer h6 {
				color: #ffffff;
			}

			.page-template-template-homepage.has-post-thumbnail .type-page.has-post-thumbnail .entry-title {
				color: #000000;
			}

			.page-template-template-homepage.has-post-thumbnail .type-page.has-post-thumbnail .entry-content {
				color: #000000;
			}

			#order_review {
				background-color: #ffffff;
			}

			#payment .payment_methods > li .payment_box,
			#payment .place-order {
				background-color: #fafafa;
			}

			#payment .payment_methods > li:not(.woocommerce-notice) {
				background-color: #f5f5f5;
			}

			#payment .payment_methods > li:not(.woocommerce-notice):hover {
				background-color: #f0f0f0;
			}

			@media screen and ( min-width: 768px ) {
				.secondary-navigation ul.menu a:hover {
					color: #ffffff;
				}

				.secondary-navigation ul.menu a {
					color: #ffffff;
				}

				.site-header-cart .widget_shopping_cart,
				.main-navigation ul.menu ul.sub-menu,
				.main-navigation ul.nav-menu ul.children {
					background-color: #000000;
				}

				.site-header-cart .widget_shopping_cart .buttons,
				.site-header-cart .widget_shopping_cart .total {
					background-color: #000000;
				}

				.site-header {
					border-bottom-color: #000000;
				}
			}.storefront-product-pagination a {
					color: #6d6d6d;
					background-color: #ffffff;
				}
				.storefront-sticky-add-to-cart {
					color: #6d6d6d;
					background-color: #ffffff;
				}

				.storefront-sticky-add-to-cart a:not(.button) {
					color: #ffffff;
				}
</style>
<link rel="stylesheet" id="storefront-icons-css" href="./detail1_files/icons.css" type="text/css" media="all">
<link rel="stylesheet" id="storefront-fonts-css" href="./detail1_files/css" type="text/css" media="all">
<link rel="stylesheet" id="storefront-jetpack-style-css" href="./detail1_files/jetpack.css" type="text/css" media="all">
<link rel="stylesheet" id="woo-viet-provinces-style-css" href="./detail1_files/provinces.css" type="text/css" media="all">
<link rel="stylesheet" id="bfa-font-awesome-css" href="./detail1_files/font-awesome.min.css" type="text/css" media="all">
<link rel="stylesheet" id="storefront-woocommerce-style-css" href="./detail1_files/woocommerce.css" type="text/css" media="all">
<style id="storefront-woocommerce-style-inline-css" type="text/css">

			a.cart-contents,
			.site-header-cart .widget_shopping_cart a {
				color: #ffffff;
			}

			table.cart td.product-remove,
			table.cart td.actions {
				border-top-color: #ffffff;
			}

			.woocommerce-tabs ul.tabs li.active a,
			ul.products li.product .price,
			.onsale,
			.widget_search form:before,
			.widget_product_search form:before {
				color: #6d6d6d;
			}

			.woocommerce-breadcrumb a,
			a.woocommerce-review-link,
			.product_meta a {
				color: #727272;
			}

			.onsale {
				border-color: #6d6d6d;
			}

			.star-rating span:before,
			.quantity .plus, .quantity .minus,
			p.stars a:hover:after,
			p.stars a:after,
			.star-rating span:before,
			#payment .payment_methods li input[type=radio]:first-child:checked+label:before {
				color: #96588a;
			}

			.widget_price_filter .ui-slider .ui-slider-range,
			.widget_price_filter .ui-slider .ui-slider-handle {
				background-color: #96588a;
			}

			.order_details {
				background-color: #f8f8f8;
			}

			.order_details > li {
				border-bottom: 1px dotted #e3e3e3;
			}

			.order_details:before,
			.order_details:after {
				background: -webkit-linear-gradient(transparent 0,transparent 0),-webkit-linear-gradient(135deg,#f8f8f8 33.33%,transparent 33.33%),-webkit-linear-gradient(45deg,#f8f8f8 33.33%,transparent 33.33%)
			}

			p.stars a:before,
			p.stars a:hover~a:before,
			p.stars.selected a.active~a:before {
				color: #6d6d6d;
			}

			p.stars.selected a.active:before,
			p.stars:hover a:before,
			p.stars.selected a:not(.active):before,
			p.stars.selected a.active:before {
				color: #96588a;
			}

			.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger {
				background-color: #eeeeee;
				color: #333333;
			}

			.single-product div.product .woocommerce-product-gallery .woocommerce-product-gallery__trigger:hover {
				background-color: #d5d5d5;
				border-color: #d5d5d5;
				color: #333333;
			}

			.button.loading {
				color: #eeeeee;
			}

			.button.loading:hover {
				background-color: #eeeeee;
			}

			.button.loading:after {
				color: #333333;
			}

			@media screen and ( min-width: 768px ) {
				.site-header-cart .widget_shopping_cart,
				.site-header .product_list_widget li .quantity {
					color: #ffffff;
				}
			}
</style>
<link rel="stylesheet" id="storefront-child-style-css" href="./detail1_files/style(1).css" type="text/css" media="all">
<script type="text/template" id="tmpl-variation-template">
	<div class="woocommerce-variation-description">{{{ data.variation.variation_description }}}</div>
	<div class="woocommerce-variation-price">{{{ data.variation.price_html }}}</div>
	<div class="woocommerce-variation-availability">{{{ data.variation.availability_html }}}</div>
</script>
<script type="text/template" id="tmpl-unavailable-variation-template">
	<p>Rất tiếc, sản phẩm này hiện không tồn tại. Hãy chọn một phương thức kết hợp khác.</p>
</script>
<script type="text/javascript" src="./detail1_files/jquery.js.download"></script>
<script type="text/javascript" src="./detail1_files/jquery-migrate.min.js.download"></script>
<link rel="https://api.w.org/" href="https://hieuapple.com/wp-json/">
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://hieuapple.com/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://hieuapple.com/wp-includes/wlwmanifest.xml"> 
<meta name="generator" content="WordPress 4.9.9">
<meta name="generator" content="WooCommerce 3.4.7">
<link rel="shortlink" href="https://hieuapple.com/?p=331">
<link rel="alternate" type="application/json+oembed" href="https://hieuapple.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhieuapple.com%2Fproduct%2Fiphone-5se-16gb.html">
<link rel="alternate" type="text/xml+oembed" href="https://hieuapple.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhieuapple.com%2Fproduct%2Fiphone-5se-16gb.html&amp;format=xml">
        <!-- Xac minh Google Search -->
         <meta name="google-site-verification" content="ReAPicwGDS7pIKvfd9Ceco6YKnkYomXP99gl1LE8ScI">
         <!-- Nhung thu vien -->
         <link rel="stylesheet" href="./detail1_files/slick.css">
         <link rel="stylesheet" href="./detail1_files/slick-theme.css">
         <script type="text/javascript" src="./detail1_files/jquery-3.3.1.min.js.download"></script>
         <script type="text/javascript" src="./detail1_files/slick.min.js.download"></script>
         <script type="text/javascript" src="./detail1_files/custom.js.download"></script>
         <link rel="icon" type="image/png" href="https://hieuapple.com/wp-content/themes/storefront-child/images/favicon.png">
         <!-- Google Analytics -->
         <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async="" src="./detail1_files/js"></script>
            <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-50130887-2');
            </script>


        <!-- Chat Facbook -->
        <!-- Load Facebook SDK for JavaScript -->


        <!-- Your customer chat code -->
        
    	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	<link rel="icon" href="https://hieuapple.com/wp-content/uploads/2018/09/cropped-favicon-1-32x32.png" sizes="32x32">
<link rel="icon" href="https://hieuapple.com/wp-content/uploads/2018/09/cropped-favicon-1-192x192.png" sizes="192x192">
<link rel="apple-touch-icon-precomposed" href="https://hieuapple.com/wp-content/uploads/2018/09/cropped-favicon-1-180x180.png">
<meta name="msapplication-TileImage" content="https://hieuapple.com/wp-content/uploads/2018/09/cropped-favicon-1-270x270.png">





<div id="page" class="hfeed site">
	         <div class="header-top">
            <div class="col-full">
                <div class="header-content">
                    <div class="hotline">
                        <span>0933.930.372 - 0962.317.214</span>
                    </div>
                    <nav>
                        <a href="https://hieuapple.com/huong-dan-tra-gop.html">Hướng dẫn trả góp</a>
                        <a href="https://hieuapple.com/chinh-sach-bao-hanh.html">Chính sách bảo hành</a>
                    </nav>
                </div>
            </div>
         </div>
        
	<header id="masthead" class="site-header" role="banner" style="">

		<div class="col-full">		<a class="skip-link screen-reader-text" href="https://hieuapple.com/product/iphone-5se-16gb.html#site-navigation">Đi đến Điều hướng</a>
		<a class="skip-link screen-reader-text" href="https://hieuapple.com/product/iphone-5se-16gb.html#content">Chuyển đến nội dung</a>
				<div class="site-branding">
			<a href="https://hieuapple.com/" class="custom-logo-link" rel="home" itemprop="url"><img src="./detail1_files/hieuapple_logo_web.svg" class="custom-logo" alt="Hiếu Apple" itemprop="logo"></a>		</div>
				    <nav class="secondary-navigation" role="navigation" aria-label="Điều hướng Phụ">
			    <div class="menu-menu-chinh-container"><ul id="menu-menu-chinh" class="menu"><li id="menu-item-83" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-product-ancestor current-menu-parent current-product-parent menu-item-has-children menu-item-83"><a href="https://hieuapple.com/danh-muc/danh-muc-iphone" class="menu-image-title-below menu-image-not-hovered"><img width="1" height="1" src="./detail1_files/icon-iphone-x.svg" class="menu-image menu-image-title-below" alt=""><span class="menu-image-title">iPhone</span></a>
<ul class="sub-menu">
	<li id="menu-item-321" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-321"><a href="https://hieuapple.com/danh-muc/danh-muc-iphone/iphone-5s" class="menu-image-title-after"><span class="menu-image-title">iPhone 5s</span></a></li>
	<li id="menu-item-118" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-118"><a href="https://hieuapple.com/danh-muc/danh-muc-iphone/iphone-6" class="menu-image-title-after"><span class="menu-image-title">iPhone 6</span></a></li>
	<li id="menu-item-119" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-119"><a href="https://hieuapple.com/danh-muc/danh-muc-iphone/iphone-7" class="menu-image-title-after"><span class="menu-image-title">iPhone 7</span></a></li>
	<li id="menu-item-116" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-116"><a href="https://hieuapple.com/danh-muc/danh-muc-iphone/iphone-8" class="menu-image-title-after"><span class="menu-image-title">iPhone 8</span></a></li>
	<li id="menu-item-117" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-117"><a href="https://hieuapple.com/danh-muc/danh-muc-iphone/iphone-x" class="menu-image-title-after"><span class="menu-image-title">iPhone X</span></a></li>
</ul>
</li>
<li id="menu-item-84" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-84"><a href="https://hieuapple.com/danh-muc/danh-muc-ipad" class="menu-image-title-below menu-image-not-hovered"><img width="1" height="1" src="./detail1_files/icon-ipad.svg" class="menu-image menu-image-title-below" alt=""><span class="menu-image-title">iPad</span></a></li>
<li id="menu-item-91" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-91"><a href="https://hieuapple.com/danh-muc/danh-muc-watch" class="menu-image-title-below menu-image-not-hovered"><img width="1" height="1" src="./detail1_files/icon-apple-watch.svg" class="menu-image menu-image-title-below" alt=""><span class="menu-image-title">Apple Watch</span></a></li>
<li id="menu-item-85" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-85"><a href="https://hieuapple.com/danh-muc/danh-muc-phu-kien" class="menu-image-title-below menu-image-not-hovered"><img width="1" height="1" src="./detail1_files/icon-airpods.svg" class="menu-image menu-image-title-below" alt=""><span class="menu-image-title">Phụ kiện</span></a></li>
<li id="menu-item-156" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-156"><a href="https://hieuapple.com/danh-muc/sua-chua" class="menu-image-title-below menu-image-not-hovered"><img width="1" height="1" src="./detail1_files/icon-iphone-fix.svg" class="menu-image menu-image-title-below" alt=""><span class="menu-image-title">Sửa chữa</span></a></li>
<li id="menu-item-230" class="menu-item menu-item-type-post_type_archive menu-item-object-product menu-item-230"><a href="https://hieuapple.com/cua-hang" class="menu-image-title-below menu-image-not-hovered"><img width="1" height="1" src="./detail1_files/icon-tat-ca-san-pham.svg" class="menu-image menu-image-title-below" alt=""><span class="menu-image-title">Tất cả sản phẩm</span></a></li>
</ul></div>		    </nav><!-- #site-navigation -->
		    			<div class="site-search">
				<div class="widget woocommerce widget_product_search"><form role="search" method="get" class="woocommerce-product-search" action="https://hieuapple.com/">
	<label class="screen-reader-text" for="woocommerce-product-search-field-0">Tìm kiếm cho:</label>
	<input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Tìm sản phẩm…" value="" name="s">
	<button type="submit" value="Tìm kiếm">Tìm kiếm</button>
	<input type="hidden" name="post_type" value="product">
</form>
</div>			</div>
		</div><div class="storefront-primary-navigation"><div class="col-full">		<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="Thanh điều hướng chính">
		<button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span>Danh mục</span></button>
			<div class="menu"><ul aria-expanded="false" class="nav-menu">
<li><a href="https://hieuapple.com/">Trang chủ</a></li><li class="page_item page-item-27 "><a href="https://hieuapple.com/blog.html">Blog</a></li>
<li class="page_item page-item-61"><a href="https://hieuapple.com/chinh-sach-bao-hanh.html">Chính sách bảo hành</a></li>
<li class="page_item page-item-5 current_page_parent"><a href="https://hieuapple.com/cua-hang.html">Cửa hàng</a></li>
<li class="page_item page-item-6"><a href="https://hieuapple.com/gio-hang.html">Giỏ hàng</a></li>
<li class="page_item page-item-64"><a href="https://hieuapple.com/gioi-thieu.html">Giới thiệu</a></li>
<li class="page_item page-item-149"><a href="https://hieuapple.com/hieu-apple-1.html">Hiếu Apple 1</a></li>
<li class="page_item page-item-151"><a href="https://hieuapple.com/hieu-apple-2.html">Hiếu Apple 2</a></li>
<li class="page_item page-item-249"><a href="https://hieuapple.com/huong-dan-tra-gop.html">Hướng dẫn trả góp</a></li>
<li class="page_item page-item-242"><a href="https://hieuapple.com/mua-hang-tra-gop.html">Mua hàng trả góp</a></li>
<li class="page_item page-item-8"><a href="https://hieuapple.com/tai-khoan.html">Tài khoản</a></li>
<li class="page_item page-item-7"><a href="https://hieuapple.com/thanh-toan.html">Thanh toán</a></li>
</ul></div>
<div class="handheld-navigation"><ul id="menu-menu-chinh-1" class="menu"><li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat current-product-ancestor current-menu-parent current-product-parent menu-item-has-children menu-item-83"><a href="https://hieuapple.com/danh-muc/danh-muc-iphone" class="menu-image-title-below menu-image-not-hovered"><img width="1" height="1" src="./detail1_files/icon-iphone-x.svg" class="menu-image menu-image-title-below" alt=""><span class="menu-image-title">iPhone</span></a><button aria-expanded="false" class="dropdown-toggle"><span class="screen-reader-text">Expand child menu</span></button>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-321"><a href="https://hieuapple.com/danh-muc/danh-muc-iphone/iphone-5s" class="menu-image-title-after"><span class="menu-image-title">iPhone 5s</span></a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-118"><a href="https://hieuapple.com/danh-muc/danh-muc-iphone/iphone-6" class="menu-image-title-after"><span class="menu-image-title">iPhone 6</span></a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-119"><a href="https://hieuapple.com/danh-muc/danh-muc-iphone/iphone-7" class="menu-image-title-after"><span class="menu-image-title">iPhone 7</span></a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-116"><a href="https://hieuapple.com/danh-muc/danh-muc-iphone/iphone-8" class="menu-image-title-after"><span class="menu-image-title">iPhone 8</span></a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-117"><a href="https://hieuapple.com/danh-muc/danh-muc-iphone/iphone-x" class="menu-image-title-after"><span class="menu-image-title">iPhone X</span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-84"><a href="https://hieuapple.com/danh-muc/danh-muc-ipad" class="menu-image-title-below menu-image-not-hovered"><img width="1" height="1" src="./detail1_files/icon-ipad.svg" class="menu-image menu-image-title-below" alt=""><span class="menu-image-title">iPad</span></a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-91"><a href="https://hieuapple.com/danh-muc/danh-muc-watch" class="menu-image-title-below menu-image-not-hovered"><img width="1" height="1" src="./detail1_files/icon-apple-watch.svg" class="menu-image menu-image-title-below" alt=""><span class="menu-image-title">Apple Watch</span></a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-85"><a href="https://hieuapple.com/danh-muc/danh-muc-phu-kien" class="menu-image-title-below menu-image-not-hovered"><img width="1" height="1" src="./detail1_files/icon-airpods.svg" class="menu-image menu-image-title-below" alt=""><span class="menu-image-title">Phụ kiện</span></a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-156"><a href="https://hieuapple.com/danh-muc/sua-chua" class="menu-image-title-below menu-image-not-hovered"><img width="1" height="1" src="./detail1_files/icon-iphone-fix.svg" class="menu-image menu-image-title-below" alt=""><span class="menu-image-title">Sửa chữa</span></a></li>
<li class="menu-item menu-item-type-post_type_archive menu-item-object-product menu-item-230"><a href="https://hieuapple.com/cua-hang" class="menu-image-title-below menu-image-not-hovered"><img width="1" height="1" src="./detail1_files/icon-tat-ca-san-pham.svg" class="menu-image menu-image-title-below" alt=""><span class="menu-image-title">Tất cả sản phẩm</span></a></li>
</ul></div>		</nav><!-- #site-navigation -->
				<ul id="site-header-cart" class="site-header-cart menu">
			<li class="">
										<a class="cart-contents" href="https://hieuapple.com/gio-hang.html" title="Xem giỏ hàng của bạn">
				<span class="woocommerce-Price-amount amount">0<span class="woocommerce-Price-currencySymbol">₫</span></span> <span class="count">0 mục</span>
			</a>
		
					</li>
			<li>
				<div class="widget woocommerce widget_shopping_cart"><div class="widget_shopping_cart_content">

	<p class="woocommerce-mini-cart__empty-message">Chưa có sản phẩm trong giỏ hàng.</p>


</div></div>			</li>
		</ul>
		</div></div>
	</header><!-- #masthead -->

	<div class="storefront-breadcrumb"><div class="col-full"><nav class="woocommerce-breadcrumb"><a href="https://hieuapple.com/">Trang chủ</a><span class="breadcrumb-separator"> / </span><a href="https://hieuapple.com/danh-muc/danh-muc-iphone">iPhone</a><span class="breadcrumb-separator"> / </span><a href="https://hieuapple.com/danh-muc/danh-muc-iphone/iphone-se">iPhone SE</a><span class="breadcrumb-separator"> / </span>iPhone SE  16GB</nav></div></div>
	<div id="content" class="site-content" tabindex="-1">
		<div class="col-full">

		<div class="woocommerce"></div>
			<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
		
		
			<div id="product-331" class="post-331 product type-product status-publish has-post-thumbnail product_cat-iphone-se product_cat-danh-muc-iphone product_tag-iphone-se first instock shipping-taxable purchasable product-type-simple">

	<div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 1; transition: opacity 0.25s ease-in-out 0s;"><a href="https://hieuapple.com/product/iphone-5se-16gb.html#" class="woocommerce-product-gallery__trigger"></a>
	<figure class="woocommerce-product-gallery__wrapper">
		<div data-thumb="https://hieuapple.com/wp-content/uploads/2018/11/192-g-se-100x100.jpg" class="woocommerce-product-gallery__image" style="position: relative; overflow: hidden;"><a href="images/<?=$row["id"]?>.jpg"><img width="400" height="400" src="./detail1_files/192-g-se.jpg" class="wp-post-image" alt="" title="192-g-se" data-caption="" data-src="https://hieuapple.com/wp-content/uploads/2018/11/192-g-se.jpg" data-large_image="https://hieuapple.com/wp-content/uploads/2018/11/192-g-se.jpg" data-large_image_width="400" data-large_image_height="400" srcset="https://hieuapple.com/wp-content/uploads/2018/11/192-g-se.jpg 400w, https://hieuapple.com/wp-content/uploads/2018/11/192-g-se-150x150.jpg 150w, https://hieuapple.com/wp-content/uploads/2018/11/192-g-se-300x300.jpg 300w, https://hieuapple.com/wp-content/uploads/2018/11/192-g-se-24x24.jpg 24w, https://hieuapple.com/wp-content/uploads/2018/11/192-g-se-36x36.jpg 36w, https://hieuapple.com/wp-content/uploads/2018/11/192-g-se-48x48.jpg 48w, https://hieuapple.com/wp-content/uploads/2018/11/192-g-se-324x324.jpg 324w, https://hieuapple.com/wp-content/uploads/2018/11/192-g-se-100x100.jpg 100w" sizes="(max-width: 400px) 100vw, 400px"></a><img role="presentation" alt="" src="images/<?=$row["id"]?>.jpg" class="zoomImg" style="position: absolute; top: -2.92263px; left: -2.70205px; opacity: 0; width: 400px; height: 400px; border: none; max-width: none; max-height: none;"></div>	</figure>
</div>

	<div class="summary entry-summary">
		<h1 class="product_title entry-title"><?=$row["name"]?></h1><p class="price"><span class="woocommerce-Price-amount amount"><?=$row["price"]?><span class="woocommerce-Price-currencySymbol">₫</span></span></p>

	
	<form class="cart" action="http://localhost:2502/customer.php?product_id=<?=$row["id"]?>" method="post" enctype="multipart/form-data">
	
	</div>
	
		<button type="submit" name="add-to-cart" value="331" class="single_add_to_cart_button button alt">Thêm vào giỏ</button>

			</form>

	
<div class="product_meta">


<table class="shop_attributes">
	
	
			<tbody><tr>
			<th>Bộ nhớ trong</th>
			<td><p><a href="https://hieuapple.com/thuoc-tinh/bo-nho-trong/16gb" rel="tag">16GB</a></p>
</td>
		</tr>
	</tbody></table>
			</div>
			</div>


</div>


		
				</main><!-- #main -->
		</div><!-- #primary -->

		
	</li>
</ul></div></div><!-- #secondary -->


	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full">

							<div class="footer-widgets row-1 col-3 fix">
							<div class="block footer-widget-1">
								<div id="custom_html-2" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="wrap-footer">
	<h3>Hệ thống cửa hàng</h3>
	<div class="box-address">
			<h4 class="title"><i class="fa fa-map-marker"></i> Nam Apple 1</h4>
		<div>Địa chỉ: 13/3 Phạm Văn Thuận, KP.1, P.Tam Hoà, TP.Biên Hoà</div>
		<div>Điện thoại: 0962.317.214 - 0933.930.372</div>
		<div>Chi tiết: <a href="https://hieuapple.com/hieu-apple-1.html" target="_blank">Xem tại đây</a></div>
	</div>
	<div class="box-address">
			<h4 class="title"><i class="fa fa-map-marker"></i> Nam Apple 2</h4>
		<div>Địa chỉ: 66 Võ Thị Sáu, P.Quyết Thắng, TP.Biên Hoà</div>
		<div>Điện thoại: 0962.317.214 - 0907.929.409</div>
		<div>Chi tiết: <a href="https://hieuapple.com/hieu-apple-2.html" target="_blank">Xem tại đây</a></div>
	</div>
</div></div></div>							</div>
							<div class="block footer-widget-2">
								<div id="custom_html-3" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="wrap-footer">
	<h3>Về chúng tôi</h3>
	<div class="box-about">
		<a href="https://hieuapple.com/gioi-thieu.html">Giới thiệu</a>
		<a href="https://hieuapple.com/chinh-sach-bao-hanh.html">Chính sách bảo hành</a>
		<a href="https://hieuapple.com/huong-dan-tra-gop.html">Hướng dẫn trả góp</a>
	</div>
</div></div></div>							</div>
							<div class="block footer-widget-3">
								<div id="custom_html-4" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><div class="wrap-footer">
	<h3>Liên kết</h3>
	<div class="box-social">
		<a href="https://www.facebook.com/HieuAppleBH/" target="_blank">
			<i class="fa fa-facebook-square"></i> 			<span>Facebook</span>
		</a>
<!-- 		<a href="#" target="_blank">
			<i class="fa fa-google-plus-square"></i> 	 
			<span>Google</span>
		</a> -->
	</div>
</div></div></div>							</div>
				</div><!-- .footer-widgets.row-1 -->		<div class="site-info">
			© Nam Apple 2018						<br>
			            <span class="author_design">Design by <a href="https://www.facebook.com/thaihoangcntt" target="_blank" rel="author">Thai Hoang</a></span>					</div><!-- .site-info -->
				<div class="storefront-handheld-footer-bar">
			<ul class="columns-2">
									<li class="search">
						<a href="https://hieuapple.com/product/iphone-5se-16gb.html">Tìm kiếm</a>			<div class="site-search">
				<div class="widget woocommerce widget_product_search"><form role="search" method="get" class="woocommerce-product-search" action="https://hieuapple.com/">
	<label class="screen-reader-text" for="woocommerce-product-search-field-1">Tìm kiếm cho:</label>
	<input type="search" id="woocommerce-product-search-field-1" class="search-field" placeholder="Tìm sản phẩm…" value="" name="s">
	<button type="submit" value="Tìm kiếm">Tìm kiếm</button>
	<input type="hidden" name="post_type" value="product">
</form>
</div>			</div>
							</li>
									<li class="cart">
												<a class="footer-cart-contents" href="https://hieuapple.com/gio-hang.html" title="Xem giỏ hàng của bạn">
				<span class="count">0</span>
			</a>
		
							</li>
							</ul>
		</div>
		
		</div><!-- .col-full -->
	</footer><!-- #colophon -->

				<section class="storefront-sticky-add-to-cart">
				<div class="col-full">
					<div class="storefront-sticky-add-to-cart__content">
						<img width="324" height="324" src="./detail1_files/192-g-se-324x324.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="">						<div class="storefront-sticky-add-to-cart__content-product-info">
							<span class="storefront-sticky-add-to-cart__content-title">You're viewing: <strong>iPhone SE  16GB</strong></span>
							<span class="storefront-sticky-add-to-cart__content-price"><span class="woocommerce-Price-amount amount">3.500.000<span class="woocommerce-Price-currencySymbol">₫</span></span></span>
													</div>
						<a href="https://hieuapple.com/product/iphone-5se-16gb.html?add-to-cart=331" class="storefront-sticky-add-to-cart__content-button button alt">
							Thêm vào giỏ						</a>
					</div>
				</div>
			</section><!-- .storefront-sticky-add-to-cart -->
		
</div><!-- #page -->


<div id="yith-quick-view-modal">

	<div class="yith-quick-view-overlay"></div>

	<div class="yith-wcqv-wrapper" style="left: 174.5px; top: 60px; width: 1000px; height: 488px;">

		<div class="yith-wcqv-main">

			<div class="yith-wcqv-head">
				<a href="https://hieuapple.com/product/iphone-5se-16gb.html#" id="yith-quick-view-close" class="yith-wcqv-close">X</a>
			</div>

			<div id="yith-quick-view-content" class="woocommerce single-product"></div>

		</div>

	</div>

</div><script type="application/ld+json">{"@context":"https:\/\/schema.org\/","@graph":[{"@context":"https:\/\/schema.org\/","@type":"BreadcrumbList","itemListElement":[{"@type":"ListItem","position":"1","item":{"name":"Trang ch\u1ee7","@id":"https:\/\/hieuapple.com"}},{"@type":"ListItem","position":"2","item":{"name":"iPhone","@id":"https:\/\/hieuapple.com\/danh-muc\/danh-muc-iphone"}},{"@type":"ListItem","position":"3","item":{"name":"iPhone SE","@id":"https:\/\/hieuapple.com\/danh-muc\/danh-muc-iphone\/iphone-se"}},{"@type":"ListItem","position":"4","item":{"name":"iPhone SE 16GB"}}]},{"@context":"https:\/\/schema.org\/","@type":"Product","@id":"https:\/\/hieuapple.com\/product\/iphone-5se-16gb.html","name":"iPhone SE 16GB","image":"https:\/\/hieuapple.com\/wp-content\/uploads\/2018\/11\/192-g-se.jpg","description":"","sku":"","offers":[{"@type":"Offer","price":"3500000","priceSpecification":{"price":"3500000","priceCurrency":"VND","valueAddedTaxIncluded":"false"},"priceCurrency":"VND","availability":"https:\/\/schema.org\/InStock","url":"https:\/\/hieuapple.com\/product\/iphone-5se-16gb.html","seller":{"@type":"Organization","name":"Hi\u1ebfu Apple","url":"https:\/\/hieuapple.com"}}]}]}</script>
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="pswp__bg"></div>
	<div class="pswp__scroll-wrap">
		<div class="pswp__container">
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
			<div class="pswp__item"></div>
		</div>
		<div class="pswp__ui pswp__ui--hidden">
			<div class="pswp__top-bar">
				<div class="pswp__counter"></div>
				<button class="pswp__button pswp__button--close" aria-label="Đóng (Esc)"></button>
				<button class="pswp__button pswp__button--share" aria-label="Chia sẻ"></button>
				<button class="pswp__button pswp__button--fs" aria-label="Bật/tắt chế độ toàn màn hình"></button>
				<button class="pswp__button pswp__button--zoom" aria-label="Phóng to/ thu nhỏ"></button>
				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
						<div class="pswp__preloader__cut">
							<div class="pswp__preloader__donut"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div>
			</div>
			<button class="pswp__button pswp__button--arrow--left" aria-label="Ảnh trước (mũi tên trái)"></button>
			<button class="pswp__button pswp__button--arrow--right" aria-label="Ảnh tiếp (mũi tên phải)"></button>
			<div class="pswp__caption">
				<div class="pswp__caption__center"></div>
			</div>
		</div>
	</div>
</div>
	<script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>
	<link rel="stylesheet" id="woocommerce_prettyPhoto_css-css" href="./detail1_files/prettyPhoto.css" type="text/css" media="all">
<script type="text/javascript">
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"Xem gi\u1ecf h\u00e0ng","cart_url":"https:\/\/hieuapple.com\/gio-hang.html","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type="text/javascript" src="./detail1_files/add-to-cart.min.js.download"></script>
<script type="text/javascript" src="./detail1_files/jquery.zoom.min.js.download"></script>
<script type="text/javascript" src="./detail1_files/jquery.flexslider.min.js.download"></script>
<script type="text/javascript" src="./detail1_files/photoswipe.min.js.download"></script>
<script type="text/javascript" src="./detail1_files/photoswipe-ui-default.min.js.download"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_single_product_params = {"i18n_required_rating_text":"Vui l\u00f2ng ch\u1ecdn m\u1ed9t m\u1ee9c \u0111\u00e1nh gi\u00e1","review_rating_required":"yes","flexslider":{"rtl":false,"animation":"slide","smoothHeight":true,"directionNav":false,"controlNav":"thumbnails","slideshow":false,"animationSpeed":500,"animationLoop":false,"allowOneSlide":false},"zoom_enabled":"1","zoom_options":[],"photoswipe_enabled":"1","photoswipe_options":{"shareEl":false,"closeOnScroll":false,"history":false,"hideAnimationDuration":0,"showAnimationDuration":0},"flexslider_enabled":"1"};
/* ]]> */
</script>
<script type="text/javascript" src="./detail1_files/single-product.min.js.download"></script>
<script type="text/javascript" src="./detail1_files/jquery.blockUI.min.js.download"></script>
<script type="text/javascript" src="./detail1_files/js.cookie.min.js.download"></script>
<script type="text/javascript">
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type="text/javascript" src="./detail1_files/woocommerce.min.js.download"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_aa2e7ccd596e4ef534b2f68a5e805bb2","fragment_name":"wc_fragments_aa2e7ccd596e4ef534b2f68a5e805bb2"};
/* ]]> */
</script>
<script type="text/javascript" src="./detail1_files/cart-fragments.min.js.download"></script>
<script type="text/javascript">
		jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
			jQuery( 'body' ).trigger( 'jetpack-lazy-images-load' );
		} );
	
</script>
<script type="text/javascript" src="./detail1_files/underscore.min.js.download"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
/* ]]> */
</script>
<script type="text/javascript" src="./detail1_files/wp-util.min.js.download"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wc_add_to_cart_variation_params = {"wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_no_matching_variations_text":"R\u1ea5t ti\u1ebfc, kh\u00f4ng c\u00f3 s\u1ea3n ph\u1ea9m n\u00e0o ph\u00f9 h\u1ee3p v\u1edbi l\u1ef1a ch\u1ecdn c\u1ee7a b\u1ea1n. H\u00e3y ch\u1ecdn m\u1ed9t ph\u01b0\u01a1ng th\u1ee9c k\u1ebft h\u1ee3p kh\u00e1c.","i18n_make_a_selection_text":"Ch\u1ecdn c\u00e1c t\u00f9y ch\u1ecdn cho s\u1ea3n ph\u1ea9m tr\u01b0\u1edbc khi cho s\u1ea3n ph\u1ea9m v\u00e0o gi\u1ecf h\u00e0ng c\u1ee7a b\u1ea1n.","i18n_unavailable_text":"R\u1ea5t ti\u1ebfc, s\u1ea3n ph\u1ea9m n\u00e0y hi\u1ec7n kh\u00f4ng t\u1ed3n t\u1ea1i. H\u00e3y ch\u1ecdn m\u1ed9t ph\u01b0\u01a1ng th\u1ee9c k\u1ebft h\u1ee3p kh\u00e1c."};
/* ]]> */
</script>
<script type="text/javascript" src="./detail1_files/add-to-cart-variation.min.js.download"></script>
<script type="text/javascript">
/* <![CDATA[ */
var yith_wccl_arg = {"is_wc24":"1"};
/* ]]> */
</script>
<script type="text/javascript" src="./detail1_files/frontend.min.js.download"></script>
<script type="text/javascript">
/* <![CDATA[ */
var yith_qv = {"ajaxurl":"\/wp-admin\/admin-ajax.php","loader":"https:\/\/hieuapple.com\/wp-content\/plugins\/yith-woocommerce-quick-view\/assets\/image\/qv-loader.gif","is2_2":"","lang":""};
/* ]]> */
</script>
<script type="text/javascript" src="./detail1_files/frontend.min.js(1).download"></script>
<script type="text/javascript">
/* <![CDATA[ */
var storefrontScreenReaderText = {"expand":"Expand child menu","collapse":"Collapse child menu"};
/* ]]> */
</script>
<script type="text/javascript" src="./detail1_files/navigation.min.js.download"></script>
<script type="text/javascript" src="./detail1_files/skip-link-focus-fix.min.js.download"></script>
<script type="text/javascript" src="./detail1_files/provinces.js.download"></script>
<script type="text/javascript" src="./detail1_files/header-cart.min.js.download"></script>
<script type="text/javascript" src="./detail1_files/wp-embed.min.js.download"></script>
<script type="text/javascript">
/* <![CDATA[ */
var storefront_sticky_add_to_cart_params = {"trigger_class":"entry-summary"};
/* ]]> */
</script>
<script type="text/javascript" src="./detail1_files/sticky-add-to-cart.min.js.download"></script>
<script type="text/javascript" src="./detail1_files/jquery.prettyPhoto.min.js.download"></script>



</body></html>
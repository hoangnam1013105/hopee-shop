
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Success</title>

   <!-- Latest compiled and minified CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->

    <!-- Custom styles for this template -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>

    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/customer.css" rel="stylesheet">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/slick.min.js"></script>
  </head>

  <body>
        <header>
            <div class="head-top">
                <div class="container">
                    <div class="head-top-left">
                      <i class="fa fa-phone" aria-hidden="true"></i>  0933.930.372 - 0962.317.214
                    </div>
                    <div class="head-side">
                      <div class="side">
                        <ul>
                          <li><a href="#">0 muc</a></li>
                          <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                        </ul>
                      </div>
                    </div>
                    <div class="head-top-right">
                      <div class="menu">
                        <ul>
                          <li><a href="#">Hướng dẫn trả góp</a></li>
                          <li><a href="#">Chính sách bảo hành</a></li>
                        </ul>
                      </div>
                    </div>
                </div>
            </div>
            <div class="head-bottom">
              <div class="container">
                <div class="brand">
                  <img src="images/hieuapple_logo_web.svg">
                </div>
                <div class="main-menu">
                    <div class="main">
                        <ul>
                          <li><a href="#">
                            <div class="dropdown">
                              <img src="images/icon-iphone-x.svg">
                              <div class="dropiphone">IPhone</div>
                              <div class="dropdown-content">
                                  <a href="#">Iphone SE</a>
                                  <a href="#">Iphone 6</a>
                                  <a href="#">Iphone 7</a>
                                  <a href="#">Iphone 8</a>
                                  <a href="#">Iphone X</a>
                              </div>
                            </div></a></li>
                          <li><a href="#"><img src="images/icon-ipad.svg"><div>Ipad</div></a></li>
                          <li><a href="#"><img src="images/icon-apple-watch.svg"><div>Apple watch</div></a></li>
                          <li><a href="#"><img src="images/icon-airpods.svg"><div>Phụ kiện</div></a></li>
                          <li><a href="#"><img src="images/icon-iphone-fix.svg"><div>Sửa chữa</div></a></li>
                          <li><a href="#"><img src="images/icon-tat-ca-san-pham.svg"><div>Tất cả sản phẩm</div></a></li>
                        </ul>
                      </div>
                </div>
                <div class="Search">
                  <div class="form">
                      <form action="https://hieuapple.com/">
                        <button type="submit"><i class="fa fa-search"></i></button>
                        <input type="text" placeholder="Search.." name="search">
                      </form>
                  </div>
                </div>
              </div>
            </div>
        </header>

        <main>
<h2>Quý khách đã đặt hàng thành công</h2>
<div><a href="http://localhost:2502/">Quay lại shop</a></div>

                <!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="../vendor/raphael/raphael.min.js"></script>
<script src="../vendor/morrisjs/morris.min.js"></script>
<script src="../data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>
        </main>

  </body>
</html>

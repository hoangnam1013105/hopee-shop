
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Customer</title>

   <!-- Latest compiled and minified CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->

    <!-- Custom styles for this template -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>

    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/customer.css" rel="stylesheet">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/slick.min.js"></script>
  </head>

  <body>
        <header>
            <div class="head-top">
                <div class="container">
                    <div class="head-top-left">
                      <i class="fa fa-phone" aria-hidden="true"></i>  0933.930.372 - 0962.317.214
                    </div>
                    <div class="head-side">
                      <div class="side">
                        <ul>
                          <li><a href="#">0 muc</a></li>
                          <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                        </ul>
                      </div>
                    </div>
                    <div class="head-top-right">
                      <div class="menu">
                        <ul>
                          <li><a href="#">Hướng dẫn trả góp</a></li>
                          <li><a href="#">Chính sách bảo hành</a></li>
                        </ul>
                      </div>
                    </div>
                </div>
            </div>
            <div class="head-bottom">
              <div class="container">
                <div class="brand">
                  <img src="images/hieuapple_logo_web.svg">
                </div>
                <div class="main-menu">
                    <div class="main">
                        <ul>
                          <li><a href="#">
                            <div class="dropdown">
                              <img src="images/icon-iphone-x.svg">
                              <div class="dropiphone">IPhone</div>
                              <div class="dropdown-content">
                                  <a href="#">Iphone SE</a>
                                  <a href="#">Iphone 6</a>
                                  <a href="#">Iphone 7</a>
                                  <a href="#">Iphone 8</a>
                                  <a href="#">Iphone X</a>
                              </div>
                            </div></a></li>
                          <li><a href="#"><img src="images/icon-ipad.svg"><div>Ipad</div></a></li>
                          <li><a href="#"><img src="images/icon-apple-watch.svg"><div>Apple watch</div></a></li>
                          <li><a href="#"><img src="images/icon-airpods.svg"><div>Phụ kiện</div></a></li>
                          <li><a href="#"><img src="images/icon-iphone-fix.svg"><div>Sửa chữa</div></a></li>
                          <li><a href="#"><img src="images/icon-tat-ca-san-pham.svg"><div>Tất cả sản phẩm</div></a></li>
                        </ul>
                      </div>
                </div>
                <div class="Search">
                  <div class="form">
                      <form action="https://hieuapple.com/">
                        <button type="submit"><i class="fa fa-search"></i></button>
                        <input type="text" placeholder="Search.." name="search">
                      </form>
                  </div>
                </div>
              </div>
            </div>
        </header>

        <main>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "iphone";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}   
$sql = "SELECT id, name, price FROM iphone where id=".intval($_GET["product_id"]);
$result = $conn->query($sql);
if ($result->num_rows != 1) {
	echo "Error";
	exit(0);
}
$row = $result->fetch_assoc();
if(isset($_POST["edit"])) {
  $customer_name = isset($_POST["name"]) ? $_POST["name"] : "";
  $product_id = null;
  $customer_email = isset($_POST["E-mail"]) ? $_POST["E-mail"] : "";
  $customer_tel = isset($_POST["Tel"]) ? $_POST["Tel"] : "";
  $customer_address = isset($_POST["Address"]) ? $_POST["Address"] : "";
  $product = null;
  if(isset($_GET["product_id"])) {
    $product_id = intval($_GET["product_id"]);
    $sql = "SELECT id, name, price FROM iphone where id=".$product_id;
    $result = $conn->query($sql);
    $product = $result->fetch_assoc();
  }

  if(!$customer_name || !$customer_email || !$customer_tel || !$customer_address || !$product){
    echo "情報を入力してください！";
    
  } else {
    $customer_name = $conn->real_escape_string($customer_name);
    $customer_email = $conn->real_escape_string($customer_email);
    $customer_tel = $conn->real_escape_string($customer_tel);
    $customer_address = $conn->real_escape_string($customer_address);
    // お客さん作成
    $sql_insert_order = "insert INTO customer(name, tel, address, email) values('$customer_name', '$customer_tel', '$customer_address', '$customer_email')";
    $res = $conn->query($sql_insert_order);
    // 注文作成
    $last_customer_id = $conn->insert_id;
    $sql_insert_order = "insert INTO `order`(customer_id) values($last_customer_id)";
    $res = $conn->query($sql_insert_order);
    //オーダーしたアイテム作成
    $last_order_id = $conn->insert_id;
    $product_name = $product["name"];
    $product_price = $product["price"];
    $sql_insert_order_item = "insert INTO `order_item`(name_item, don_gia, ngay_order, order_id) values('$product_name', '$product_price', now(), $last_order_id)";
    $res = $conn->query($sql_insert_order_item);
    header("Location: /thanhcong.php");
    die();
    
  }
}
?>
            <section>
                <div class="container">
                <div class="col-lg-12">
                  <h2 class="section-title">Order</h2>
                  <div class="col-md-4">
                    <div class="iphone-show">
                      <ul>
                        <li>
                          <a href="#">
                            <div class="img-product"><img src="/images/<?=$row["id"]?>.jpg"></div>
                          </a>
                        </li>    
                      </ul>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="hienthi">
                      <h2 class="hienthi1"><?=$row["name"]?></h2>
                      <div class="hienthi2"><?=$row["price"]?>₫</div>
                    </div>
                  </div>
                  <div class="col-md-4">
                            <form method='POST' action="" class="customer-details">
                                Customer's Name: <input name="name" class="form-control" value="" />
                                Tel: <input name="Tel" class="form-control" value="" />
                                Address: <input name="Address" class="form-control" value="" />
                                E-mail: <input name="E-mail" class="form-control" value="" />
                            
                            <div class="edit">
                              
                                <input type="hidden" name="edit" value="1" />
                                <input type="submit" value="Finish" class="btn btn-success"/>
                           
                            </div>
                            </div>
                  </div>
                </div>
              </div> 
            </section> 

                <!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="../vendor/raphael/raphael.min.js"></script>
<script src="../vendor/morrisjs/morris.min.js"></script>
<script src="../data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>
        </main>
            
        <footer>

          <div class="container">

            <div class="store">
                <h3>Hệ Thống Cửa Hàng</h3>
                <h4>
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    Nam táo 1
                </h4>
                <div>Địa chỉ: 13/3 Phạm Văn Thuận, KP.1, P.Tam Hoà, TP.Biên Hoà</div>
                <div>Điện thoại: 0962.317.214 - 0933.930.372</div>
                <div>Chi tiết: <a href="https://hieuapple.com/hieu-apple-2.html">Xem tại đây.</a></div>
                <h4>
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    Nam táo 2
                </h4>
                <div>Địa chỉ: 13/3 Phạm Văn Thuận, KP.1, P.Tam Hoà, TP.Biên Hoà</div>
                <div>Điện thoại: 0962.317.214 - 0933.930.372</div>
                <div>Chi tiết: <a href="https://hieuapple.com/hieu-apple-2.html">Xem tại đây.</a></div>
            </div>

            <div class="lien-he">
                <h3>About Us</h3>
                <div><a href="https://hieuapple.com/gioi-thieu.html">Giới thiệu</a></div>
                <div><a href="https://hieuapple.com/chinh-sach-bao-hanh.html">chính sách bảo hành</a></div>
                <div><a href="https://hieuapple.com/huong-dan-tra-gop.html">Hướng dẫn trả góp</a></div>
            </div>

            <div class="face">
                <h3>Liên kết</h3>
                <h4>
                  <i class="fa fa-facebook-square" aria-hidden="true"></i><a href="https://www.facebook.com/">Facebook</a>
                </h4>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="logo-web">
              <div>© Nam Apple 2018</div>
              <div>Design by Anonymous</div>
          </div>

        </footer>

        <script type="text/javascript">
          $(document).ready(function(){
            $('#banner_ads').slick({
              dots: true,
              arrows: true,
            });
          });
        </script>
  </body>
</html>

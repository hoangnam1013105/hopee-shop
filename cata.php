
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>cata</title>

   <!-- Latest compiled and minified CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->

    <!-- Custom styles for this template -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
    <link href="css/shop-item.css" rel="stylesheet">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/slick.min.js"></script>
  </head>

  <body>

  <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "iphone";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
$sql = "SELECT id, name, price FROM iphone";
if(isset($_GET["search"] ) && $_GET["search"]) {
  $search = $conn->real_escape_string($_GET["search"]);
  $sql = "SELECT id, name, price FROM iphone where name like '%".$search."%'";
}
$result = $conn->query($sql);

?>
        <header>
            <div class="head-top">
                <div class="container">
                    <div class="head-top-left">
                      <i class="fa fa-phone" aria-hidden="true"></i>  0933.930.372 - 0962.317.214
                    </div>
                    <div class="head-side">
                      <div class="side">
                        <ul>
                          <li><a href="#">0 muc</a></li>
                          <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                        </ul>
                      </div>
                    </div>
                    <div class="head-top-right">
                      <div class="menu">
                        <ul>
                          <li><a href="#">Hướng dẫn trả góp</a></li>
                          <li><a href="#">Chính sách bảo hành</a></li>
                        </ul>
                      </div>
                    </div>
                </div>
            </div>
            <div class="head-bottom">
              <div class="container">
                <div class="brand">
                  <img src="images/hieuapple_logo_web.svg">
                </div>
                <div class="main-menu">
                    <div class="main">
                        <ul>
                          <li><a href="#">
                            <div class="dropdown">
                              <img src="images/icon-iphone-x.svg">
                              <div class="dropiphone">IPhone</div>
                              <div class="dropdown-content">
                                  <a href="#">Iphone SE</a>
                                  <a href="#">Iphone 6</a>
                                  <a href="#">Iphone 7</a>
                                  <a href="#">Iphone 8</a>
                                  <a href="#">Iphone X</a>
                              </div>
                            </div></a></li>
                          <li><a href="#"><img src="images/icon-ipad.svg"><div>Ipad</div></a></li>
                          <li><a href="#"><img src="images/icon-apple-watch.svg"><div>Apple watch</div></a></li>
                          <li><a href="#"><img src="images/icon-airpods.svg"><div>Phụ kiện</div></a></li>
                          <li><a href="#"><img src="images/icon-iphone-fix.svg"><div>Sửa chữa</div></a></li>
                          <li><a href="#"><img src="images/icon-tat-ca-san-pham.svg"><div>Tất cả sản phẩm</div></a></li>
                        </ul>
                      </div>
                </div>
                <div class="Search">
                  <div class="form">
                      <form action="https://hieuapple.com/">
                        <button type="submit"><i class="fa fa-search"></i></button>
                        <input type="text" placeholder="Search.." name="search">
                      </form>
                  </div>
                </div>
              </div>
            </div>
        </header>

        <main>      
            <section>
                <div class="container">
                  <h2 class="section-title">iphone</h2>
                    <div class="iphone-show">
                      <ul>
                      <?php if ($result->num_rows > 0) {
							// output data of each row
							while($row = $result->fetch_assoc()) {
					?>
					<li>
                        <a href="http://localhost:2502/detail.php?id=<?=$row["id"]?>">
                          <div class="img-product"><img src="images/<?=$row["id"]?>.jpg"></div>
                          <div class="name-product"><?=$row["name"]?></div>
                          <div class="price"><?=$row["price"]?>₫</div>
                        </a>
                      </li>
					<?php
							} //end while
						} // endif
					?>
                      </ul>
                    </div>
                </div>
            </section> 
        </main>
            
        <footer>

          <div class="container">

            <div class="store">
                <h3>Hệ Thống Cửa Hàng</h3>
                <h4>
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    Nam táo 1
                </h4>
                <div>Địa chỉ: 13/3 Phạm Văn Thuận, KP.1, P.Tam Hoà, TP.Biên Hoà</div>
                <div>Điện thoại: 0962.317.214 - 0933.930.372</div>
                <div>Chi tiết: <a href="https://hieuapple.com/hieu-apple-2.html">Xem tại đây.</a></div>
                <h4>
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    Nam táo 2
                </h4>
                <div>Địa chỉ: 13/3 Phạm Văn Thuận, KP.1, P.Tam Hoà, TP.Biên Hoà</div>
                <div>Điện thoại: 0962.317.214 - 0933.930.372</div>
                <div>Chi tiết: <a href="https://hieuapple.com/hieu-apple-2.html">Xem tại đây.</a></div>
            </div>

            <div class="lien-he">
                <h3>About Us</h3>
                <div><a href="https://hieuapple.com/gioi-thieu.html">Giới thiệu</a></div>
                <div><a href="https://hieuapple.com/chinh-sach-bao-hanh.html">chính sách bảo hành</a></div>
                <div><a href="https://hieuapple.com/huong-dan-tra-gop.html">Hướng dẫn trả góp</a></div>
            </div>

            <div class="face">
                <h3>Liên kết</h3>
                <h4>
                  <i class="fa fa-facebook-square" aria-hidden="true"></i><a href="https://www.facebook.com/">Facebook</a>
                </h4>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="logo-web">
              <div>© Hiếu Apple 2018</div>
              <div>Design by Anonymous</div>
          </div>

        </footer>

        <script type="text/javascript">
          $(document).ready(function(){
            $('#banner_ads').slick({
              dots: true,
              arrows: true,
            });
          });
        </script>
  </body>
</html>
